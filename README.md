rcon.js
=======

*rcon.js* allows you to send commands to Rcon-enabled servers.

Requirements
============

* RingoJS

How to Use
==========

See the examples in the `examples/` folder.
